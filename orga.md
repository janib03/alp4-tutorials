# Organization

This document contains all of the organisational information for my tutorials.

## Contact Me

E-Mail: chao.zhan@fu-berlin.de

Gitlab issues: https://git.imp.fu-berlin.de/mactavish96/alp4-tutorials/-/issues

## Tutorial Sessions

- Thursday 8 am - 10 am at T9/051 Seminarraum
- Friday 12 pm - 2 pm at T9/053 Seminarraum

## Active Participation

- Participate in group discussions
- Present your solution to one of the questions from the assignments at least twice (officially, but adjustable)

## Assignments

- For programming assignments, commit your code on GitLab
- For writing assignments, use the latex template

The ideal form of submissions will be: upload your PDF via the Whiteboard together with the link to your assignment branch on GitLab.

## Miscellaneous

If possible, bring your personal laptop with you. There will be some small programming exercises from time to time.
