#include "linked_list.h"

struct list_node {
   struct list_node *prev, *next;
   ll_data_t data;
};

struct list {
   struct list_node *first, *last;
};

struct list_node *create_node(ll_data_t data) {
    // TODO: implement create_node
}

// constructs a new (empty) list
struct list *list_create(void) {
    // TODO: implement list_create
}

// counts the items on a list
size_t list_count(const struct list *list) {
    // TODO: implement list_count
}

// inserts item at back of a list
void list_push(struct list *list, ll_data_t item_data) {
    // TODO: implement list_push
}

// removes item from back of a list
ll_data_t list_pop(struct list *list) {
    // TODO: implement list_pop
}

// inserts item at front of a list
void list_unshift(struct list *list, ll_data_t item_data) {
  // TODO: implement list_unshift
}

// removes item from front of a list
ll_data_t list_shift(struct list *list) {
    // TODO: implement list_shift
}

// deletes a node that holds the matching data
void list_delete(struct list *list, ll_data_t data) {
    // TODO: implement list_delete
}

// destroys an entire list
// list will be a dangling pointer after calling this method on it
void list_destroy(struct list *list) {
    // TODO: implement list_destroy
}
