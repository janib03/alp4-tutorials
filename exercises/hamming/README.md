# Hamming

Here is a small exercise that helps you understand pointer arithmetic and String in C.

## Instructions

Calculate the Hamming Distance between two DNA sequences.

If we compare two sequences of DNA and count the differences between them we can see how many mismatches occurred.

This is known as the "Hamming Distance".

We read DNA using the letters C,A,G and T. Two sequences might look like this:

```
GAGCCTACTAACGGGAT
CATCGTAATGACGGCCT
^ ^ ^  ^ ^    ^^
```

They have 7 differences, and therefore the Hamming Distance is 7.

## Implementation notes

The Hamming distance is only defined for sequences of equal length, so an attempt to calculate it between sequences of different lengths should not work.

## Tips

You could use auxiliary functions such as `strlen` to get the length of a given string. These functions are defined in `<string.h>`

## Testing

The included makefile can be used to create and run the tests using the following commands:

```bash
# run unit tests
make test

# check memory leaks
make memcheck

# clean all compilation files
make clean
```

## Credit

Adapted from [here](https://github.com/exercism/c/tree/main/exercises/practice/hamming)
