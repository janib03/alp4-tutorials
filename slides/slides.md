---
# try also 'default' to start simple
theme: seriph
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: true
# persist drawings in exports and build
drawings:
  persist: false
# page transition
transition: fade-out
# use UnoCSS
css: unocss
---

# ALP4 Tutorial 1

## Chao Zhan

<div class="abs-br m-6 flex gap-2">
  <a href="https://github.com/slidevjs/slidev" target="_blank" alt="GitHub"
    class="text-xl icon-btn opacity-50 !border-none !hover:text-white">
    <carbon-logo-github />
  </a>
</div>

<!---->

---
transition: slide-left
src: ./pages/intro.md
---

---
transition: slide-left
src: ./pages/orga.md
---

---
transition: slide-left
src: ./pages/env.md
---

---
transition: slide-left
src: ./pages/git.md
---

---
transition: slide-left
src: ./pages/clang-1.md
---

---
transition: slide-left
src: ./pages/clang-2.md
---

---
transition: slide-left
src: ./pages/clang-3.md
---

---
transition: slide-left
src: ./pages/clang-4.md
---

---
transition: slide-left
src: ./pages/clang-5.md
---

---
transition: slide-left
src: ./pages/materials.md
---
