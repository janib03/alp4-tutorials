---
title: C Programming Language review
layout: two-cols
---

# C Programming Language Review

C is a **system programming language**, created for implementing the **Unix** operating system.

## Spirit of C

- Trust the programmer (run fast and crash fast)
- Keep the language small and simple
- Fast and portable (Scientific computation in python pretty fast? Well, check out the [implementation](https://github.com/python/cpython))

K&R is just about a **must-have**

::right::

<div class="container flex justify-center">
    <img src="/images/k-and-r.jpg" class="block w-xs"/>
</div>
