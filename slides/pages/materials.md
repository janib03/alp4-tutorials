---
title: Useful Materials
layout: two-cols
---

# Notes

- Cover other tools such as `gdb` and `make` next time (depends on the demand)
- Unit Testing in C

## Other Useful Resources

- [The Missing Semester of Your CS Education](https://missing.csail.mit.edu/)
- [DevDocs](https://devdocs.io/)
- [tldr](https://github.com/tldr-pages/tldr)

