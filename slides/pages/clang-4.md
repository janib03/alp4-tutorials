---
title: C Memory Management
layout: two-cols
---

# C Memory Management

Program's address space contains **stack**, **heap**, **static data**, **code**. Dynamic memory management refers to heap memory management.

### Support Functions in `<stdlib.h>`

- `malloc()`: allocate a block of **uninitialized** memory
- `calloc()`: allocate a block of **zeroed** memory
- `free()`: free previously allocated memory
- `realloc()`: expand or shrink the previously allocated memory

### Tools

Use [valgrind](https://valgrind.org/) to find out memory related bugs.

::right::

<div class="container flex justify-center">
    <img src="/images/program-address-space.png" class="block w-xs"/>
</div>

