---
title: Development Environment
---

# Development Environment Setup

- Access to Linux Pool (Andorra)
- SSH Key Authentication - Safe and quick

<div class="container flex justify-center mt-xs mb-xs">
    <img src="/images/ssh-key-auth-flow.png" class="block w-m"/>
</div>

- Verify `git` and `gcc` or `clang` Installation

## Configure SSH ()

Follow the steps in: https://git.imp.fu-berlin.de/mactavish96/alp4-tutorials/-/blob/tutorial-1/env.md
