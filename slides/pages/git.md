---
title: Git Basics
layout: two-cols
---

# Git Basics

- Create a new repo: `git init`
- Clone an existing repo: `git clone <repo url>`
- Making Changes:
    - Propose changes: `git add <filename>`
    - Commit changes: `git commit -m "your commit message"`
- Pushing Changes: `git push origin main`
- Pulling Changes: `git pull origin main`

## Git With Me

Live demo

::right::

<div class="container flex justify-center mt-25">
    <img src="/images/git-three-states.png" class="block w-xs"/>
</div>

