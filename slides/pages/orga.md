---
title: Organization
---

# Organization

E-Mail: chao.zhan@fu-berlin.de

### Active Participation

- Participate in group discussions
- Present your solution to one of the questions from the assignments at least twice (officially, but adjustable)

<br />

### Assignments

- For programming assignments, commit your code on GitLab
- Code should be able to compiled and executed on Andorra
- For writing assignments, please use the latex template

<br />

### Roles

- Mine: Offering helps, answering questions
- Yours: Thinking & Getting your hands dirty (coding, tooling)
