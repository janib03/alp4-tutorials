---
title: Dynamic Analysis
---

# Dynamic Analysis

There are tools that help you automatically detect memory management and threading bugs.

- [Valgrind](https://valgrind.org/) (not available on lower version of OS X and Windows)
- [Sanitizer](https://github.com/google/sanitizers) (integrated into `gcc` and `clang`)

## Basic Usage

```bash
# Using AddressSanitizer - dectect memory errors, memory leaks etc.
clang -fsanitize=address -g -O2 -fno-omit-frame-pointer <your c source file>

# Using MemorySanitizer - detect uninitialized memory read
clang -fsanitize=memory -fPIE -pie -fno-omit-frame-pointer -g -O2 <your c source file>

# Using Valgrind, you need to first compile your program
valgrind --leak-check=full --show-leak-kinds=all <program> <arg1> <arg2> ...
```
