---
title: C Key Concepts
---

# C Key Concepts

- Pointers
- Arrays
- Implications for Memory Management

All of the above are **unsafe**. You can shoot yourself in the foot easily. Full of **undefined behaviours** -- source of the most errors.

#### Understanding The Compilation Process

<div class="container flex justify-center mt-5">
    <img src="/images/c-compilation.webp" class="block w-1/2"/>
</div>

