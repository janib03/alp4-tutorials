---
title: Pointers in C
layout: two-cols
---

# Pointers in C

Consider memory to be a huge single array.

- Each cell has an address associated with it.
- Each cell can store some value.
- **Pointers** are cells that contains the address of a cell.
- Pointers have **type** (they point to this type of data).
- Pointers can point to almost anything: variables, pointers, even functions.
- **Null Pointer** is special, don't read or write it. (Undefined behaviour!)
- **Array variables are almost identical to pointers**.
- Array in function arguments is just a pointer without **size**
- String in C is just an array of `char`.

::right::

```c
/* Basic Syntax */
int *ptr; int a = 10;
ptr = &a; *ptr = 20;

/* Pointer to a Pointer */
int **pptr = &ptr;
**pptr = 30;

/* Function Pointer */
int a = 10; int b = 20;
int fn(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}
int (*swap) (int *, int *) = &fn;
(*swap)(&a, &b);

/* Array and Pointer */
int arr[3] = {1, 2, 3};
// arr[1] == *(a + 1)

/* String */
char str1[] = "I am a character array";
char *str2 = "I am a string literal";
```
