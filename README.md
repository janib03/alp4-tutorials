# ALP4 Tutorial-1

This branch contains all materials for the first tutorial session.

## Agenda

- Greetings and self-introduction
- [Organization](./orga.md)
- [Development environment setup](./env.md)
- [Git Basics](./git.md)
- [C programming language review](./clang.md)
- Questions for the first assignment
- Groups for assignments

## Usage

Please read the separate markdown files accordingly for detailed information.
