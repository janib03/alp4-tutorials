# Git Basics

In this document, we will go over some fundamental Git concepts and how to use them. Since Git is a relative huge and complex software and it has lots of commands, we only covers the basics in this document.

## What is Git

Git is a free and open source **distributed version control software** created by Linus Torvalds.

It is the most popular version control system (VCS) in the world. Most of the open source projects are managed using Git.

Being distributed means that every developer working with Git has a copy of that entire repository, which enables the user to coordinate and synchronize their work on different machines.

## Getting Started with Git

In this section, we provide a friendly overview of Git.

### Installing Git

If you are using MacOS or one of the Linux distributions, then you should already have Git installed on you machine. If you are using Windows or doesn't have Git installed on your machine, then you can read this [guide](https://www.atlassian.com/git/tutorials/install-git) which provides lots of useful information about how to install Git on your machine.

Then you can test your Git installation using the following command:

```sh
git --version
```

### The Core of Git - Git Repository

A Git repository, often with the shorter name **repo**, is just a place that stores all your project files where you can save versions of your code, documentations, configurations etc.

There are two types of repositories, one is local repository, the other is central repository. As the names suggested, the local repository is store on your local machine, and the central repository is store on a remote server.

![](./images/git-repo.svg)

### Prepare Yourself

It is a good idea to add some basic information of yourself to Git before doing any operation, so that later when you collaborate with other people you can easily find out who did what operations. The easiest way to do so is:

```sh
git config --global user.name "Your Name"
git config --global user.email "you@yourdomain.example.com"
```

The is actually a global level configuration operation, which is user-specific in your operating system. But you can change that at any time.

### Creating a New Repository Locally

To create a local Git repo, just type the command in any working directory you prefer:

```sh
cd /path/to/your/existing/project
git init
```

This will turn the directory a Git repository, it will create a new `.git` subdirectory in your current working directory.

Once you have a central repo on the server, you will need to add the remote repo url to you Git configuratio using:

```sh
git remote add <remote name> <remote repo url>
```

### Cloning an Existing Repository

If your project has already been set up in a central repository, then you can obtain a local copy of it using:

```sh
git clone <repo url>
```

`<repo url>` can be an URL over HTTPS or SSH etc. And you can clone the remote repository to a specific folder:

```sh
git clone <repo url> <local directory>
```

A typical SSH URL looks like this: `git@HOSTNAME:USERNAME/REPONAME.git`.

### Saving Changes

When working wit Git, the concept of of **saving** is a more subtle process than saving in a traditional file editing application such as Microsoft Word.

In Git, the correct term of saving is **"committing"**. A **commit** is the Git equivalent of a "save". Git commits can be done locally then pushed to a remote server as need using the command:

```sh
# if you are pushing the local branch to the remote target for the first time after using git init
git push -u <remote name> <local branch>

# if you've used git clone to set up your repo, this will send the current branch to its default remote counterpart
git push
```
Don't worry, we will come back to `git push` later.

#### The Three States

Before we start to talk about how to commit changes to the repository, we need to remember this very concept in Git, which is, Git has **three** main states that your files can reside in: **modified**, **staged** and **committed**:

![](./images/git-three-states.png)

#### Modified in Working Directory

The working directory is where you are working on you project, you can change the content of any file and save it using any text editor. The file is **modified** but Git is not aware of it yet.

After changing anything, you can use the command `git status` to see which files are changed. You are likely to see message like this:

```sh
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   <YOU CHANGED FILE>
```

You can also use `git diff` to show all changes that haven't been staged yet. It will give you a comparison view of the file before and after your edits.

#### Staged in Staging Area

If you wish to let Git know the change of a certain file for the next commit, you will have to use the `git add` command, for example:

```sh
# let say, you've just add some lines in hello.c
git add hello.c

# or you can add all modifled files at once
git add .
#
git add --all
```

This will add your changes to the files to the staging area. Now you can use the command `git status` again to see the difference:

```sh
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   hello.c
```

This means that, Git now is aware of the change to this file and ready to "commit (save)" it. However, `git add` needs to be called **every time you alter a file**, otherwise later changes won't be noticed if you try to commit them later.

#### Committed in the Repository

Now, you can commit your changes using the `git commit` command, this will launch a text editor prompting you for a **commit message**. You should enter a message describing what you've done, save the file and close the editor to create the actual commit.

A shortcut command that immediately creates a commit with a passed message is:

```sh
# passing -m option will forgo the text editor prompt
git commit -m "your commit message"
```

Normally, `git commit` will commit all staged files to the repository, but you can commit only specific files (but they still need to be already staged):

```sh
# commit specific files
git commit <path/to/file1> <path/to/file2>
```

After committing you changes to the repository, you can run `git status` again assuming that you haven't made any new changes in the working directory, you will likely to see a message like this:

```sh
nothing to commit, working tree clean
```

Now, you can use `git log` command to show information about the commit.

### More on Git Commits 

Every change you commit will be viewable in the respective file or directory in chronological order.

![](./images/git-commit.png)

Git records the entire contents of each file in every commit, it's referred to as **snapshot**.

### Collaborating

Git's distributed collaboration model gives every developer their own copy of the repository, complete with its own local history and branch structure.

Users typically need to share a series of commits rather than a single changeset. Instead of committing a changeset from a working copy to the central repository, Git lets you share entire branches between repositories.

In this chapter, you will learn how to sync your work with other people.

#### Git remote

The `git remote` command lets you create, view, and delete connections to other repositories.

You can use it to view your git remote configuration:

```sh
# list remote connections to other repos with name
git remote
# same as above but with concrete urls
git remote -v
```

You can create, modify and examine your git remote configurations easily:

```sh
# create a new connection to a remote repo
git remote add <remote name> <remote url>

# rename a existing remote connection
git remote rename <old-name> <new-name>

# remove the connetion to the remote repo
git remote rm <name>

# look for detailed information of a remote repo
git remote show <remote name>

# get the url of a remote repo
git remote get-url <remote name>
```

Once a remote record has been configured through the use of the git remote command, the remote name can be passed as an argument to other Git commands to communicate with the remote repo.

##### The origin Remote

When you clone a repository with `git clone`, it automatically creates a remote connection called `origin` pointing back to the cloned repository. 

This is useful for developers creating a local copy of a central repository, since it provides an easy way to pull upstream changes or publish local commits.

#### Fetching and Pulling from Git Remotes

After setting up the remote connection to a central repo, you can use `git fetch` to download commits, files and refs etc. from a remote repo into your local repo.

Git isolates fetched content from existing local content; it has absolutely no effect on your local development work.

In contrast, `git pull` is a more aggressive alternative; it will download the remote content for the active local branch and immediately execute `git merge` to create a merge commit for the new remote content.

We will talk about **branches** and command like `git merge` in Git repo in detail. For now, you can use `git fetch` in the following ways:

```sh
# fetch all branches from the remote repo
git fetch <remote>

# fetch only from a specific branch
git fetch <remote> <branch>

# fetch all registered remote repos
git fetch --all
```

Remember that, `git pull` is basically equivalent to `git fetch` + `git merge`.

#### Pushing to Git Remotes

The `git push` command is used to upload local repository content to a remote repository.

Pushing is how you transfer commits from your local repository to a remote repo. It's the counterpart of `git fetch`.

You can use `git push` in the following ways:

```sh
# push your current branch to the <branch> in the remote repo
# Git will prevent you from doing this, if it is not a fast-foward merge in the destination repository
git push <remote> <branch>

# push all of your local branches to the specific remote
git push <remote> --all
```

Now I will show you some voodoo magic, you can use the `--force` flag to overwrite anything in the remote branch:

```sh
# for example, this will overwrite the branch in the remote, and possibly causing inconsistency
git push <remote> --force
```

**DO NOT USE THIS FLAG**, unless you're **absolutely** sure of what you're doing.

### Using Branches

In Git, branches are a part of your everyday development process. Git branches are effectively a **pointer** to a snapshot of your changes.

When you want to add a new feature or fix a bug—no matter how big or how small, you should create a new branch to encapsulate your changes.

![](./images/git-branch.svg)

The figure above depicts a repository of three different branches, they are all independent to each other.

The `git branch` command lets you create, list, rename, and delete branches. But it doesn't allow you to switch between branches, fork a new branch, or even merge branches back together. For this reason, `git branch` is often used in conjunction with `git checkout` and `git merge` commands.

#### Basic Branch Operations

```sh
# list all your branches
git branch

# create a new branch, but will not switch to the newly created branch
git branch <new branch name>

# delete a branch
git branch -d <branch name>

# rename the current branch
git branch -m <new branch name>

# list all remote branches
git branch -a
```

It's important to know that, creating a new branch does not change the repo; it simply creates a new pointer that points to the commit:

![](./images/git-create-branch.png)

The illustration above provides a visual on what happens when the branch is created. The repository is the same, but a new pointer is added to the current commit.

#### Switching Branches

`git checkout` lets you navigates between the branches created by `git branch`. Checking out a branch updates the files in the working directory.

You can switch to a branch easily using:

```sh
git checkout <branch name>
```

Additionally, you can use `git checkout` to create a new branch from the current commit (pointed by a special pointer called **HEAD**) and switch to that newly created branch immediate using `-b` flag:

```sh
git checkout -b <new branch name>

# or you can checkout a new branch from some other branch
git checkout -b <new branch name> <some other exisiting branch>
```

#### Tracking Branches

Lets say you are working on a branch named `dev` which you fetched from the remote named `origin`. Now you want to work on this branch for a quite a long time and want the `git push` and `git pull` using the `dev` branch of `origin` by default. So that you won't have to type `git push origin dev` or `git pull origin dev` every time you want to update or pull the updates to and from the remote branch.

First, you need to tell Git which branch to track:

```c
# suppose you are on the dev branch on you local machine, and there is already a dev branch in the remote repo
git branch --set-upstream-to origin/remote
```

This only works when there is already a `dev` branch in the remote repo. If you want to create a new branch in the remote repo, and track it automatically, then you can use the following command:

```
# this will create a dev branch in the remote and track it using the current branch locally
git push --set-upstream origin dev
# or, same as above
git push -u origin dev
```

#### Merging Branches

Merging in Git is the way to putting forked history back together again. The `git merge` command lets you integrate branches created by `git branch` into a single branch.

In most cases, `git merge` is used to combine two branches. It takes two commit pointers, usually the branch tips, and will find a common base commit between them. Then `git merge` will create a special `merge commit` that combines the changes from both branches.

Take a look at the following example:

![](./images/git-merge-1.png)

Assuming that we are currently at the `main` branch, if we invoke `git merge <branch name>`, the branch will be merged into the `main` branch:

![](./images/git-merge-2.png)

This is also called a 3-way merge, because it involves the `current branch`, the `branch you want to merge`, as well as the `common ancestor`.

##### Fast Forward Merge

A fast forward merge occurs when there is a linear path from the current branch to the branch to be merged ahead. This is the easiest situation for Git, it would only have to move the pointer to the tip of the merging branch:

![](./images/fast-forward-merge.svg)

The fast forward merge is only possible when the branches are not diverged.

#### Resolving Conflicts

If there is a merge operation, then there will most likely be a merge conflict.

When the two branches you are trying to merge both changed the same of a file, Git won't be able to figure out which version to use. When this situation arises, Git simply stops merging and let you resolve the merge conflicts. 

The great part of Git's merging process is that it uses the familiar edit/stage/commit workflow to resolve merge conflicts.

When you encounter a merge conflict, running the `git status` command shows you which files need to be resolved.

For example, if both branches modified the same section of `hello.c`:

```sh
On branch main
Unmerged paths:
(use "git add/rm ..." as appropriate to mark resolution)
both modified: hello.c
```

##### Understanding the Conflicts

When Git encounters a conflict during a merge, It will edit the content of the affected files with visual indicators that mark both sides of the conflicted content. 

These visual markers are: <<<<<<<, =======, and >>>>>>>. You will likely to see something similar like this in the file that has caused the conflict:

```sh
here is some content not affected by the conflict
<<<<<<< main
this is conflicted text from main
=======
this is conflicted text from feature branch
>>>>>>> feature branch;
```

You can edit the file as you like, resolve the conflicts in a way that suits your need (e.g. delete all the contents). Then you will have to invoke `git add` and `git commit` to finish the merge.

##### Aborting the Merge

However, sometimes you might want to cancel the merge. You can abort the merge in case of conflicts using this command:

```sh
git merge --abort
```

This won't help you resolve the conflicts but it will rollback to the state before invoking `git merge`.

### Git Workflow

There is a variety of of branching strategies and workflows. A well structured workflow promotes organization, efficiency and code quality.

Here we will focus on the **Feature Branch Workflow** which is simple to learn and to use.

In short, developers create separate branches for each feature or bugfix, keeping the ‘main’ branch stable. 
When a feature is complete, the developer submits a `pull request` or `merge request` to integrate the changes back into the main branch after a code review.

#### Pull/Merge Request

`Pull request` or `merge request` is usually provided by the collaborating platform such as GitLab and GitHub. A pull request notifies other development team members of changes made in your local repository.

Here is a simple workflow with pull request that you and your team can follow:

![](./images/pull-request.png)

Since we are using GitLab, you can read the documentation [here](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) and learn how to create merge requests on GitLab.

## Common Misconceptions

### Github/Gitlab/etc. != Git

Git is only a software whereas Github or Gitlab is a commercial online platform which provides services that host your git repository remotely.

### Snapshots, Not Differences

The major difference between Git and any other VCS is the way Git thinks about its data.

With Git, every time you commit, or save the state of your project, Git basically takes a picture of what all your files look like at that moment and stores a reference to that snapshot.

If files have not changed, Git doesn’t store the file again, just a link to the previous identical file it has already stored.

## Git Clients

After knowing the basic concepts of Git, you can choose whether to stick with the command-line interface or to use other Git-based clients.

We suggest you first familiarize yourself with the command-line; after that, you can switch to any other tools you like without any troubles.

### GUI Clients

If you prefer to use a GUI client, here are some options:

- [Github Desktop](https://desktop.github.com/)
- [Sourcetree](https://www.sourcetreeapp.com/)
- [GitLens in VS Code](https://gitlens.amod.io/)

### Terminal Clients

If you prefer to use a terminal based client, here are some options:

- [Tig](https://jonas.github.io/tig/)
- [GitUI](https://github.com/extrawurst/gitui)
- [LazyGit](https://github.com/jesseduffield/lazygit)

## Git Cheat Sheets

- [DE](https://training.github.com/downloads/de/github-git-cheat-sheet/)
- [EN](https://training.github.com/downloads/github-git-cheat-sheet/)

## Other Useful Learning Materials

- https://blinry.itch.io/oh-my-git
- https://github.com/initialcommit-com/git-sim
- https://learn.microsoft.com/en-us/training/modules/intro-to-git/ 
- https://git-scm.com/book/en/v2

### References

- https://git-scm.com/doc
- https://github.com/git-guides
- https://www.atlassian.com/git/tutorials
- https://nulab.com/learn/software-development/git-tutorial/git-basics/
