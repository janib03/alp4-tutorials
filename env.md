# Development environment setup 

This document contains all of the information your need to get started with your development environment.

## Access to Linux-pool

You will need to have an institutional account in order to have access to the Linux-pool (Andorra).

Go to https://portal.mi.fu-berlin.de/login and login in with your ZEDAT account. If you don't have a ZEDAT account yet, you need to reach out to [ZEDAT](https://www.zedat.fu-berlin.de/Home) or [IT-Services](http://www.mi.fu-berlin.de/w/IT).

Further information can be found here: http://www.mi.fu-berlin.de/w/IT/Computeraccess

## Configuring SSH-Key-Based Authentication

After setting up your account, let's configure SSH-key-based authentication for you to have a secure way of connecting to the Linux-Pool(Andorra) and to the Gitlab at FU (via `git`).

The instructions described below only work for Linux-distributions or Unix-like operating systems. For those who are using Windows, please check Microsoft's online documentation for SSH on Windows: https://learn.microsoft.com/en-us/windows-server/administration/openssh/openssh_overview

Alternatively, you can use WSL(Windows Subsystem for Linux), please check Microsoft's online documentation: https://learn.microsoft.com/en-us/windows/wsl/install

### Overview of SSH-Key-Based Authentication

An SSH server can authenticate clients using a variety of different methods. The most basic of these is password authentication, which is easy to use, but not the most secure.

SSH Keys is proved to be a reliable and secure alternative.

SSH key pairs are two cryptographically secure keys that can be used to authenticate a client to an SSH server. Each key pair consists of a **public key** and a **private key**.

The private key is retained by the client and should be **kept absolutely secret**.

The associated public key can be shared freely without any negative consequences. The public key can be used to encrypt messages that only the private key can decrypt.

The public key is uploaded to a remote server that you want to be able to log into with SSH. The key is added to a special file within the user account you will be logging into called `~/.ssh/authorized_keys`

![](./images/ssh-key-auth-flow.png)


### Step 1: Generating SSH Keys

First, open you terminal and enter the following command:

```sh
# create .ssh folder in home directory if needed and cd into it
mkdir -p ~/.ssh && cd ~/.ssh
# command to generate key-pair using recommended ed25519 algorithm
ssh-keygen -t ed25519
```

then you should see the following output:

```sh
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/username/.ssh/id_ed25519):
```

if you press enter directly, the default key-pair will be saved in your user's home directory under the name `id_ed25519` and `id_ed25519.pub`, which, are the private key and the public key.

It's recommended to give it a reasonable name to your keys, so that you can recall it later when you need to manage multiple key-pairs for different servers in the future.

Let's assume that, you given the name `fu`, and then you will see the following output:

```sh
Enter passphrase (empty for no passphrase): [Type a passphrase]
Enter same passphrase again: [Type passphrase again]
```

you can set a passphrase for the key, it's optional. If you enter one, you will have to provide it every time you use this key (unless you are running SSH agent software that stores the decrypted key). If you do not want to set a passphrase, you can press ENTER to bypass this prompt.

You now have a public and private key that you can use to authenticate with output like this:

```sh
Your identification has been saved in /home/username/.ssh/fu
Your public key has been saved in /home/username/.ssh/fu.pub
The key fingerprint is:
SHA256:L2ZhMFacpJhR9q+1eZOo/vJfIDge6sP13l0zoeihGb4 username@hostname
The key's randomart image is:
+--[ED25519 256]--+
|    ..ooo.       |
|     = +o        |
|    o = .        |
|     . o o       |
|        S + .  . |
|       +.B =.o. .|
|     ...*o=o+..o.|
|     .oooo*o.+ .o|
|      .ooE=oo .  |
+----[SHA256]-----+
```

### Step 2: Adding the SSH Public Key to Your Server

The simplest way to copy your public key to an existing server is to use a utility called `ssh-copy-id`. Because of its simplicity, this method is recommended if available.

The `ssh-copy-id` program is included in the OpenSSH packages in many distributions, so you may already have it available on your local system.

Now, use the following command to copy your SSH public key to the target server:

```sh
# make sure you are still in the directory where the keys were generated
# replace [ZEDAT_USER_NAME] with your own ZEDAT account
ssh-copy-id -i fu.pub [ZEDAT_USER_NAME]@andorra.imp.fu-berlin.de
```

You might see an output like this:

```sh
The authenticity of host '203.0.113.1 (203.0.113.1)' can't be established.
ECDSA key fingerprint is fd:fd:d4:f9:77:fe:73:84:e1:55:00:ad:d6:6d:22:fe.
Are you sure you want to continue connecting (yes/no)? yes
```

This means that your local computer does not recognize the remote host. This will happen the first time you connect to a new host. Type yes and press ENTER to continue. And you will be asked to provide the password of your ZEDAT account:

```sh
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
ZEDAT_USER_NAME@andorra.imp.fu-berlin.de's password:
```

After providing the correct password, it will then copy the contents of your `~/.ssh/fu.pub` key into a file in the remote account’s home `~/.ssh` directory called `authorized_keys`.

If you don't have `ssh-copy-id` available, you could copy your SSH public key manually using the following command:

```
# make sure you are loacted in the ~/.ssh directory
cat ~/.ssh/fu.pub | ssh [ZEDAT_USER_NAME]@andorra.imp.fu-berlin.de "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
```
This will copy the content of your public key and append it to the `~/.ssh/authorized_keys` on the remote server.

### Step 3: Connecting to Your Server Using SSH Keys

If you have successfully completed the procedures above, you should be able to log into the remote host without the remote account’s password.

```sh
# since ssh uses the default private key under the name ~/.ssh/id_rsa, ~/.ssh/id_dsa etc.
# We have to use -i to select the private key we just generated explicitly, but we will fix that later
ssh -i ~/.ssh/fu [ZEDAT_USER_NAME]@andorra.imp.fu-berlin.de
```

If this is your first time connecting to this host (if you used the last method above), you may see something like this:

```sh
The authenticity of host '203.0.113.1 (203.0.113.1)' can't be established.
ECDSA key fingerprint is fd:fd:d4:f9:77:fe:73:84:e1:55:00:ad:d6:6d:22:fe.
Are you sure you want to continue connecting (yes/no)? yes
```

This means that your local computer does not recognize the remote host. Type yes and then press ENTER to continue.

If you did not supply a passphrase for your private key, you will be logged in immediately. If you supplied a passphrase for the private key when you created the key, you will be required to enter it now. Afterwards, a new shell session will be created for you with the account on the remote system.

### Step 4: Saving Your SSH options in a Configuration file

Typing `ssh -i identity_file username@hostname` to connect to your target server is quite verbose, we want to create a kind of shortcut to memorize these options so that we can set up the connection quickly without worrying about the details.

Now, let's create a configuration file using the following command:

```sh
touch ~/.ssh/config
```

Then open the file using any text editor you prefer, e.g. `vi ~/.ssh/config` or `nano ~/.ssh.config` and add the following content to the configuration file:

```sh
# change [ZEDAT_USER_NAME] to your own ZEDAT account

# This option is only useful in MacOS, ignore it in other OS
IgnoreUnknown UseKeychain
    UseKeychain yes

# For andorra
Host andorra
  HostName andorra.imp.fu-berlin.de
  User [ZEDAT_USER_NAME]
  IdentityFile ~/.ssh/fu

# For gitlab at FU
Host git.imp.fu-berlin.de
  User git
  IdentityFile ~/.ssh/fu

# Wildcard
Host *
  # add the keys automatically to a running ssh-agent
  AddKeysToAgent yes
  UseKeychain yes
```

We are not going to discuss the syntax of the configuration file here, you can read the man page using `man ssh_config` for further information.

Now, save the file and then test the connection using the following command:

```
# test the connection to Andorra, you make have to invoke CTRL-C to quit
ssh -T andorra
```

Congratulations, from now on, you can always set up a new secure connection between your local device and the Linux-Pool using the command `ssh andorra`. (For Gitlab we won't set up the connection explicitly, but via the `git` command)


## Access to Gitlab at FU

Go to https://git.imp.fu-berlin.de/users/sign_in and sign in with you ZEDAT account and fill in your personal information.

You will be using Gitlab intensively for later assignments, read [this markdown file](./git.md) for more information about Git.

Later we are going to use the `git` command via SSH to manipulate the git repository hosted by Gitlab at FU. But we still need to add our public key to Gitlab, so that we can manipulate the remote repository without using password authentication.

### Adding your SSH Key to your Gitlab account

Just like storing our public key on the andorra server, we need to add our public key to our Gitlab account:

1. Sign in to [GitLab](https://git.imp.fu-berlin.de/).
2. On the top bar, in the upper-right corner, select your avatar.
3. Select **Edit profile**.
4. On the left sidebar, select **SSH Keys**.
5. In the Key box, paste the contents of your public key (use `cat ~/.ssh/fu.pub` to print the contents).
6. Optional. Type a description in the **Title** box, Select the **Usage type** and update the **Expiration date** (clear the field to set the key to be permanent).
7. Add the key

Now, you can run the following command to test the connection to the Gitlab:

```
# test the connection
ssh -T git@git.imp.fu-berlin.de
# try to clone this repository
git clone git@git.imp.fu-berlin.de:mactavish96/alp4-tutorials.git
```

## Installing Git and GCC/Clang

For the assignments you will need to use `git` to submit and `gcc` or `clang` to compile and test the your program.

If you are using MacOS or one of the many Linux-distributions, you should already have both commands available on your machine.

If you are using WSL on Windows, you might need to install `gcc` separately:

```sh
# assuming you are using WSL Ubuntu
sudo apt-get update
sudo apt-get install gcc
```

You can verify if those programs are already available on your machine using the following commands:

```sh
git --version
gcc --version
# or
cc --version
```

Alternatively, you can write your programs directly on Andorra, all these tools are already available to you.

## Troubleshooting

### Gitlab related

Check: https://docs.gitlab.com/ee/user/ssh.html#troubleshooting

## References

- https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server
- https://docs.gitlab.com/ee/user/ssh.html
- https://docs.github.com/en/authentication/connecting-to-github-with-ssh/about-ssh
